;
; Minecraft Remaps v2
; Author:         Desi Quintans <me@desiquintans.com>
; Website:         http://www.desiquintans.com
;
; Script Function:
;  The following only apply inside the Minecraft window:
;   1) Mouse Button 4 performs a left-click.
;   2) F1 toggles hold-left-click. Handy for breaking lots of blocks or mining obsidian.
;   3) F2 toggles hold-W, making you move forward automatically. Use with F1 for automated mining action!
;   4) LCtrl toggles crouching upon double-pressing it. Press it again (once) to turn off.
;  The following only applies if IrfanView is running.
;   5) PrintScreen -> Ctrl+F11 for taking screenies via IrfanView.
;

#NoEnv
SendMode Input
SetWorkingDir %A_ScriptDir%

#IfWinActive, Minecraft
{
   XButton1::LButton ; Remaps button 4 to left-click

   ; The following autopilot code was borrowed from jaceguay at http://www.autohotkey.com/forum/topic59506.html
   F1::Send % "{LButton " ((Cnt := !Cnt) ? "Down}" : "Up}" )
   F2::Send % "{w " ((Cnt2 := !Cnt2) ? "Down}" : "Up}" )

   ;The following crouch-toggle code was borrowed from Lanser at http://www.autohotkey.com/forum/topic16058.html
   ~LCtrl up::
     Goto, Crouch
   
   Crouch:
     If (a_tickCount-lasttime < 400)
     {
       Loop
       {
         Send, {LCtrl down}
         If IsKeyPressed("LCtrl")
           Send, {LCtrl up}
         Break
       }
     }
     lasttime:=a_tickCount
   Return
   
   IsKeyPressed(v_KeyName)
     {
       GetKeyState, state, %v_KeyName%, P
       If state = D
       {
         Return 1
       }
       Return 0
     }
}

#IfWinExist, IrfanView
{
   PrintScreen::^F11 ;Remaps Ctrl+F11 to PrintScreen for easy screenies via IrfanView.
}
