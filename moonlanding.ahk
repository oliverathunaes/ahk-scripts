
; Launching various applications

!+^1::
    IfWinExist ahk_exe dopus.exe
        WinActivate ahk_exe dopus.exe
    else
        Run C:\Program Files\GPSoftware\Directory Opus\dopus.exe
return

!+^2::
    IfWinExist ahk_exe chrome.exe
        WinActivate ahk_exe chrome.exe
    else
        Run C:\Program Files (x86)\Google\Chrome\Application\chrome.exe
return

!+^3::
    IfWinExist ahk_exe nvim-qt.exe
        WinActivate ahk_exe nvim-qt.exe
    else
        Run C:\Neovim5.5\bin\nvim-qt.exe
return

!+^4::
    IfWinExist ahk_exe slack.exe
        WinActivate ahk_exe slack.exe
    else
        Run C:\Users\Oliver\AppData\Local\slack\slack.exe
return

!+^5::
    IfWinExist ahk_exe Discord.exe
        WinActivate ahk_exe Discord.exe
    else
        Run C:\Users\Oliver\AppData\Local\Discord\app-1.0.9002\Discord.exe
return

!+^6::
    IfWinExist ahk_exe Spotify.exe
        WinActivate ahk_exe Spotify.exe
    else
        Run C:\Users\Oliver\AppData\Roaming\Spotify\Spotify.exe
return

!+^7::
    IfWinExist ahk_exe WinSCP.exe
        WinActivate ahk_exe WinSCP.exe
    else
        Run C:\Program Files (x86)\WinSCP\WinSCP.exe
return

; Macros

!+^F1::
    Send <?php{Space}
return

; Only for emergencies!

!+^F24::
    url = "https://www.youtube.com/watch?v=KxGRhd_iWuE"
    run % "C:\Program Files (x86)\Google\Chrome\Application\chrome.exe" ( winExist("ahk_class Chrome_WidgetWin_1") ? " --new-window " : " " ) url
    Sleep 2000
    Send f
return
